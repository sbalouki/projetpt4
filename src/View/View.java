package View;

import Controleur.Controleur;
import Model.Arete;
import Model.Model;
import Model.Noeud;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.*;
import javax.swing.*;
import javax.swing.text.Segment;
import java.lang.*;
import sun.java2d.loops.CompositeType;
import sun.java2d.loops.DrawLine;
import sun.java2d.loops.SurfaceType;
/**
 *
 * @author sbalouki
 */
public class View extends javax.swing.JPanel implements MouseListener, MouseMotionListener, KeyListener, Observer {

// <editor-fold defaultstate="collapsed" desc="Attributs de la classe Graphe">

    Model model;
    Controleur control;
    private Point pointPresse;
    private Point pointSouris;
    private JPopupMenu menuDeroulant = new JPopupMenu();
    private JMenuItem supprimer = new JMenuItem("Supprimer");
    private JMenuItem supprimerSelect = new JMenuItem("Supprimer la sélection");
    private JMenuItem copier = new JMenuItem("Copier");
    private JMenuItem coller = new JMenuItem("Coller");

    private Rectangle rect = new Rectangle();
    //</editor-fold>

// <editor-fold defaultstate="collapsed" desc="Constructeur avec Listeners">
    public View(Model mod, Controleur cont) {
        
        this.setVisible(true);
        this.setBackground(Color.white);
        model = mod;
        control = cont;
        model.addObserver(this);
        
        supprimer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                control.supprimerObjClique(pointPresse);
            }
        });
        supprimerSelect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                control.supprimerObjSelect();
            }
        });
        copier.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                control.copierSelection();
            }
        });
        coller.addActionListener(new ActionListener(){
        @Override
            public void actionPerformed(ActionEvent ae) {
                control.collerSelection(pointPresse);
            }
    });
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addKeyListener(this);
    }
    //</editor-fold>

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        Graphics2D g2D=(Graphics2D)g;
        for (Noeud n : model.getNoeuds()) 
        {
            n.setSelect(n.isSelect());
            g2D.setColor(n.getCol());
            g2D.draw(n.getForme());
            g2D.drawString(n.getEtiquette(), n.getPosX(), n.getPosY()-n.getTaille()/2-1);
        }
            for(Arete a : model.getAretes())
            {
                a.setSelect(a.isSelect());
                g2D.setColor(a.getCol());
                g2D.drawString(a.getEtiquette(), a.getPosition().x, a.getPosition().y);
                g2D.drawLine(a.getSource().getPosition().x, a.getSource().getPosition().y, a.getDestination().getPosition().x, a.getDestination().getPosition().y);
            }
        g2D.setColor(Color.blue);
        g2D.draw(rect);
    }

    @Override
    public void mouseClicked(MouseEvent me) 
    {
        if(SwingUtilities.isLeftMouseButton(me))
        {
            if(!control.dansNoeud(me.getPoint()))
            {    
                if(control.surArete(me.getPoint()))
                    control.selectionAreteSimple();
                else control.clique(me.getPoint());
            }
            else
            {
                control.selectionNoeudSimple();
            }
        }
        if(SwingUtilities.isRightMouseButton(me))
        {
                menuDeroulant.add(copier);
                menuDeroulant.add(coller);
                if(control.getNoeudsCopie().isEmpty())
                    coller.setEnabled(false);
                control.presse(pointPresse);
                pointPresse = me.getPoint();
                copier.setEnabled(false);
            if(control.dansNoeud(me.getPoint()) || control.surArete(me.getPoint()))
            {
                copier.setEnabled(true);
                coller.setEnabled(true);
                menuDeroulant.add(supprimerSelect);
                menuDeroulant.add(copier);
                menuDeroulant.add(coller);
            }
                menuDeroulant.show(this, me.getX(), me.getY());
                this.setVisible(true);
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
        if(SwingUtilities.isLeftMouseButton(me))
        {
            control.presse(me.getPoint());
            pointPresse = me.getPoint();
        }
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        rect = new Rectangle();
        if(control.dansNoeud(me.getPoint()))
        {
            pointSouris = me.getPoint();
            control.tracerArete(pointSouris);
        }
        if(!control.isMovable() )
        {
            control.setTmp(null);
            control.setArtTmp(null);
        }
        repaint();
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        
    }

    @Override
    public void mouseExited(MouseEvent me) {
        
    }

    @Override
    public void mouseDragged(MouseEvent me) {
        pointSouris = me.getPoint();
        if(SwingUtilities.isLeftMouseButton(me))
        {
            if(!control.isMovable())
            {
                rect = new Rectangle(pointPresse);
                rect.add(me.getPoint());
                control.selectionMultiple(rect);
            }
            else
            {
                if(control.getMode())
                {
                    control.deplacement(pointSouris);
                }
            }
        }
        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent me) {
        
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        
    }

    @Override
    public void update(Observable o, Object o1) {
        this.repaint();
    }
}
