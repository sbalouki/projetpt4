/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import Model.Noeud;
import com.sun.org.apache.xerces.internal.util.DOMUtil;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;

/**
 *
 * @author sbalouki
 */
public class Arete {
    // <editor-fold defaultstate="collapsed" desc="Déclaration des attributs">
    
    private Shape surfaceSelection;


    private int _taille;
    private boolean _select;
    private int _id;
    private static int nbAretes = 0;


    private String _etiquette;

    private int _posX;
    private int _posY;

    private Color _col;
    private Noeud _source;
    private Noeud _destination;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Constructeur">
    public Arete(int taille, Color col, Noeud source, Noeud destination) {
        
        _id=nbAretes++;
        _etiquette="a" + _id;
        _select = false;
        _taille = taille;
        _col = col;
        _source = source;
        _source.getAretesNoeud().add(this);
        _destination = destination;
        _destination.getAretesNoeud().add(this);
        surfaceSelection = new Rectangle(this.getPosition().x-10, this.getPosition().y-20, 30,30);
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters">
    public int getTaille() {
        return _taille;
    }

    public boolean isSelect() {
        return _select;
    }

    public Point getPosition() {
        Point position = new Point((_source.getPosX()+_destination.getPosX())/2, (_source.getPosY()+_destination.getPosY())/2);
        return position;
    }

    public Color getCol() {
        return _col;
    }

    public Noeud getSource() {
        return _source;
    }

    public Noeud getDestination() {
        return _destination;
    }
    
    public int getId() {
        return _id;
    }
    
    public String getEtiquette() {
        return _etiquette;
    }

    public Shape getSurfaceSelection() {
        return surfaceSelection;
    }
    
    public static int getNbAretes() {
        return nbAretes;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Setters">
    public void setSelect(boolean select) {
        this._select = select;
        if(_select)
            this._col=Color.RED;
        else
            this._col=Color.BLUE;
    }

    public void setTaille(int _taille) {
        this._taille = _taille;
    }

    public void setPosX(int _posX) {
        this._posX = _posX;
    }

    public void setPosY(int _posY) {
        this._posY = _posY;
    }

    public void setCol(Color _col) {
        this._col = _col;
    }

    public void setSource(Noeud _source) {
        this._source = _source;
    }

    public void setDestination(Noeud _destination) {
        this._destination = _destination;
    }
    
    public void setEtiquette(String _etiquette) {
        this._etiquette = _etiquette;
    }
    
    public void setId(int _id) {
        this._id = _id;
    }
    
    public static void setNbAretes(int nbAretes) {
        Arete.nbAretes = nbAretes;
    }
    
    public void setSurfaceSelection(Shape surfaceSelection) {
        this.surfaceSelection = surfaceSelection;
    }
    //</editor-fold>
}
