/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.util.LinkedList;
/**
 *
 * @author sbalouki
 */
public class Noeud {
    // <editor-fold defaultstate="collapsed" desc="Déclaration des attributs">
    private String _formeNom;
    private Shape _formeShape;
    private boolean _select;
    private static int nbNoeud = 0;


    private int _id;

    private Point _position;
    private int _taille;

    private Color _col;
    private String etiquette;


    private LinkedList<Arete> aretesNoeud;

//</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Constructeurs">
    public Noeud(){}
    public Noeud(Noeud n){
        this.aretesNoeud = new LinkedList<Arete>();
        this._col = n.getCol();
        this._position = new Point(n.getPosition());
        this.etiquette = n.getEtiquette() + "(copie)";
        this._id=n.getId();
        this._taille=n.getTaille();
        this._formeNom = n.getFormeNom();
        
        if(_formeNom == "Rectangle")
            _formeShape = new Rectangle(_position.x-_taille/2,_position.y-_taille/2,this._taille, this._taille);
        if(_formeNom ==  "Rond"){
            _formeShape = new Ellipse2D.Double(_position.x-_taille/2,_position.y-_taille/2,this._taille, this._taille);
        }
        if(_formeNom == "Triangle"){
            Polygon p = new Polygon();
            p.addPoint(_position.x, _position.y-this._taille/3-_taille/2);
            p.addPoint(_position.x-_taille/2, _position.y+this._taille/2);
            p.addPoint(_position.x+this._taille/2, _position.y+this._taille/2);
            _formeShape = p;
        }
    }

    public Noeud(int taille, Color col, String formeNom, Point position) {
        _id=nbNoeud++;
        etiquette = "n" + _id;
        this._formeNom = formeNom;
        this._taille = taille;
        aretesNoeud = new LinkedList<Arete>();
        this._col = col;
        this._position=position;
        _select = false ;
        
        
        if(formeNom == "Rectangle")
            _formeShape = new Rectangle(this._position.x-taille/2,this._position.y-taille/2,this._taille, this._taille);
        if(formeNom ==  "Rond"){
            _formeShape = new Ellipse2D.Double(this._position.x-taille/2,this._position.y-taille/2,this._taille, this._taille);
        }
        if(formeNom == "Triangle"){
            Polygon p = new Polygon();
            p.addPoint(_position.x, _position.y-this._taille/3-_taille/2);
            p.addPoint(_position.x-_taille/2, _position.y+this._taille/2);
            p.addPoint(_position.x+this._taille/2, _position.y+this._taille/2);
            _formeShape = p;
        }
    }//</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters">
    public int getPosX(){return _position.x;}
    public int getPosY(){return _position.y;}
    public Shape getForme(){return _formeShape;}
    public Color getCol() {return _col;}
    public Point getPosition(){return _position;}
    public boolean isSelect() {return _select;}
    public int getTaille() {return _taille;}
    public int getId() {return _id;}
    public LinkedList<Arete> getAretesNoeud() {return aretesNoeud;}
    public static int getNbNoeud() {return nbNoeud;}
    public String getEtiquette() {return etiquette;}
    public String getFormeNom() {return _formeNom;}
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Setters">
    public void setForme(String forme) {
        this._formeNom = forme;
        if(_formeNom == "Rectangle")
            _formeShape = new Rectangle(this._position.x-_taille/2,this._position.y-_taille/2,this._taille, this._taille);
        if(_formeNom ==  "Rond"){
            _formeShape = new Ellipse2D.Double(this._position.x-_taille/2,this._position.y-_taille/2,this._taille, this._taille);
        }
        if(_formeNom == "Triangle"){
            Polygon p = new Polygon();
            p.addPoint(_position.x, _position.y-this._taille/3-_taille/2);
            p.addPoint(_position.x-_taille/2, _position.y+this._taille/2);
            p.addPoint(_position.x+this._taille/2, _position.y+this._taille/2);
            this._formeShape = p;
        }
    }
    public void setCol(Color col) {this._col = col;}
    public void setSelect(boolean select) {
        this._select = select;
        if(_select)
            this._col=Color.RED;
        else
            this._col=Color.BLACK;
    }

    public void setPosition(Point position) {
        this._position = position;
        if(_formeNom == "Rectangle")
            _formeShape = new Rectangle(this._position.x-_taille/2,this._position.y-_taille/2,this._taille, this._taille);
        if(_formeNom ==  "Rond"){
            _formeShape = new Ellipse2D.Double(this._position.x-_taille/2,this._position.y-_taille/2,this._taille, this._taille);
        }
        if(_formeNom == "Triangle"){
            Polygon p = new Polygon();
            p.addPoint(_position.x, _position.y-this._taille/3-_taille/2);
            p.addPoint(_position.x-_taille/2, _position.y+this._taille/2);
            p.addPoint(_position.x+this._taille/2, _position.y+this._taille/2);
            this._formeShape = p;
        }
    }

    public void setAretesNoeud(LinkedList<Arete> aretesNoeud) {
        this.aretesNoeud = aretesNoeud;
    }

    public void setTaille(int taille) {
        this._taille=taille;
        if(_formeNom == "Rectangle")
            _formeShape = new Rectangle(this._position.x-_taille/2,this._position.y-_taille/2,this._taille, this._taille);
        if(_formeNom ==  "Rond"){
            _formeShape = new Ellipse2D.Double(this._position.x-_taille/2,this._position.y-_taille/2,this._taille, this._taille);
        }
        if(_formeNom == "Triangle"){
            Polygon p = new Polygon();
            p.addPoint(_position.x, _position.y-this._taille/3-_taille/2);
            p.addPoint(_position.x-_taille/2, _position.y+this._taille/2);
            p.addPoint(_position.x+this._taille/2, _position.y+this._taille/2);
            this._formeShape = p;
        }
    }
    
    public void setPosX(int posX) {
        this._position.x = posX;
        if(_formeNom == "Rectangle")
            _formeShape = new Rectangle(this._position.x-_taille/2,this._position.y-_taille/2,this._taille, this._taille);
        if(_formeNom ==  "Rond"){
            _formeShape = new Ellipse2D.Double(this._position.x-_taille/2,this._position.y-_taille/2,this._taille, this._taille);
        }
        if(_formeNom == "Triangle"){
            Polygon p = new Polygon();
            p.addPoint(_position.x, _position.y-this._taille/3-_taille/2);
            p.addPoint(_position.x-_taille/2, _position.y+this._taille/2);
            p.addPoint(_position.x+this._taille/2, _position.y+this._taille/2);
            this._formeShape = p;
        }
    }

    public void setPosY(int posY) {
        this._position.y = posY;
        if(_formeNom == "Rectangle")
            _formeShape = new Rectangle(this._position.x-_taille/2,this._position.y-_taille/2,this._taille, this._taille);
        if(_formeNom ==  "Rond"){
            _formeShape = new Ellipse2D.Double(this._position.x-_taille/2,this._position.y-_taille/2,this._taille, this._taille);
        }
        if(_formeNom == "Triangle"){
            Polygon p = new Polygon();
            p.addPoint(_position.x, _position.y-this._taille/3-_taille/2);
            p.addPoint(_position.x-_taille/2, _position.y+this._taille/2);
            p.addPoint(_position.x+this._taille/2, _position.y+this._taille/2);
            this._formeShape = p;
        }
    }
    public static void setNbNoeud(int _nbNoeud) {
        Noeud.nbNoeud = _nbNoeud;
    }
    public void setEtiquette(String etiquette) {etiquette = etiquette;}
    // </editor-fold>
}
