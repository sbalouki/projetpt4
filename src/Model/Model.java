/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Vector;

/**
 *
 * @author sbalouki
 */
public class Model extends Observable {

    // <editor-fold defaultstate="collapsed" desc="Déclaration de variables">
    private LinkedList<Noeud> noeuds ;
    private LinkedList<Noeud> selectionNoeuds ;
    private LinkedList<Arete> selectionAretes ;
    private LinkedList<Arete> aretes ;
    private LinkedList<Noeud> copieNoeuds;
    private int taille = 20;
    private String formeParDefaut = "Rectangle";


    private boolean modeDeplacement = false;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Constructeur">
    public Model()
    {
        noeuds = new LinkedList<Noeud>();
        aretes = new LinkedList<Arete>();
        selectionNoeuds = new LinkedList<Noeud>();
        selectionAretes = new LinkedList<Arete>();
        copieNoeuds = new LinkedList<Noeud>();
    }//</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters">

    
    // <editor-fold defaultstate="collapsed" desc="Getters des listes">
    public LinkedList<Noeud> getNoeuds() {
        return noeuds;
    }

    public LinkedList<Arete> getAretes() {
        return aretes;
    }

    public LinkedList<Noeud> getSelectionNoeuds() {
        return selectionNoeuds;
    }
    
    public LinkedList<Arete> getSelectionAretes() {
        return selectionAretes;
    }

    public LinkedList<Noeud> getCopieNoeuds() {
        return copieNoeuds;
    }
//</editor-fold>
    
    public boolean isModeDeplacement() {
        return modeDeplacement;
    }

    public String getFormeParDefaut() {
        return "Rectangle";
    }
    
    public Color getCouleurParDefaut() {
        return Color.BLACK;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Setters">
    public void setModeDeplacement(boolean modeDeplacement) {
        this.modeDeplacement = modeDeplacement;
    }

    public void setForme(String forme)
    {
        formeParDefaut = forme;
        for (Noeud noeud : noeuds) {
            if (noeud.isSelect()) {
                noeud.setForme(forme);
            }
        }
        setChanged();
        notifyObservers();        
    }

    public void setTaille(int newTaille)
    {
        taille=newTaille;   
        for (Noeud noeud : noeuds) {
            if (noeud.isSelect()) {
                noeud.setTaille(newTaille);
            }
        }  
        setChanged();
        notifyObservers();   
    }

    public void setTailleSelection(int newTaille)
    {
        for (Noeud noeud : noeuds) {
            if (noeud.isSelect()) {
                noeud.setTaille(newTaille);
            }
        }
        setChanged();
        notifyObservers();  
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Initialiser le graphe">
    public void initialiser() {
        taille = 20;
        formeParDefaut = "Rectangle";
        aretes.clear();
        selectionNoeuds.clear();
        noeuds.clear();
        Noeud.setNbNoeud(0);
        Arete.setNbAretes(0);
        
        setChanged();
        notifyObservers();
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Initialiser la sélection">
    public void initSelection() {
        selectionNoeuds.clear();
        selectionAretes.clear();
        for(int i=0;i<noeuds.size(); i++)
        {
            if(noeuds.get(i).isSelect())
                noeuds.get(i).setSelect(false);
        }
        for(int i=0; i<aretes.size(); i++)
        {
            if(aretes.get(i).isSelect())
                aretes.get(i).setSelect(false);
        }
        setChanged();
        notifyObservers();
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Tests sur les clics">
    public boolean cliqueDansNoeud(Point coord) {
        Iterator <Noeud> it = noeuds.iterator();
        while (it.hasNext()){
            Noeud tmp = it.next();
            if (tmp.getForme().contains(coord)) 
            {
                return true;
            }
        }
        return false;
    }

    public boolean cliqueSurArete(Point pointClique) {
        Iterator <Arete> it = aretes.iterator();
        while (it.hasNext()){
            Arete tmp = it.next();
            if (tmp.getSurfaceSelection().contains(pointClique)) 
            {
                return true;
            }
        }
        return false;
    }

    public Noeud noeudChoisi(Point coord) {
        Iterator <Noeud> it = noeuds.iterator();
        while (it.hasNext()){
            Noeud tmp = it.next();
            if (tmp.getForme().contains(coord))
            {
                return tmp;
            }
        }
        return null;   
    }

    public Arete areteChoisie(Point coord) {
        Iterator <Arete> it = aretes.iterator();
        while (it.hasNext()){
            Arete tmp = it.next();
            if (tmp.getSurfaceSelection().contains(coord))
            {
                return tmp;
            }
        }
        return null;
    }//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Tests sur la possibilité de tracer une arete entre deux noeuds (source, destination)">
    public boolean noeudDifferent(Noeud source, Noeud destination)
    {
        if(source!=destination)
        {
            return true;
        }
        return false;
    }
    
    public boolean areteDejaExistante(Noeud source, Noeud destination)
    {
        Iterator <Arete> it = aretes.iterator();
        while (it.hasNext()){
            Arete tmp = it.next();
            if(tmp.getSource() == source && tmp.getDestination() == destination 
            || tmp.getSource() == destination && tmp.getDestination()== source)
            {
                return true;
            }
        }
        return false;
    }
    //</editor-fold>
    
    public void supNoeud() {
        int limiteNoeud = noeuds.size()-1;
        for(int i = limiteNoeud; i >= 0; i--)
        {
            if(noeuds.get(i).isSelect())
            {
                retirerNoeudSelection(noeuds.get(i));
                aretes.removeAll(noeuds.get(i).getAretesNoeud());
                noeuds.remove(i);
            }
        }
        setChanged();
        notifyObservers();
    }

    public void supArete() {
        int limiteNoeud = aretes.size()-1;
        for(int i = limiteNoeud; i >= 0; i--)
        {
            if(aretes.get(i).isSelect())
            {
                retirerAreteSelection(aretes.get(i));
                aretes.remove(i);
            }
        }
        setChanged();
        notifyObservers();
    }

    //<editor-fold defaultstate="collapsed" desc="Ajout d'éléments">
    public void ajouterNoeud(Point p)
    {
        noeuds.add(new Noeud(taille, Color.BLACK, formeParDefaut, p));
        setChanged();
        notifyObservers();
    }

    public void ajouterArete(Noeud source, Noeud destination) {
        if(noeudDifferent(source, destination))
        {
            if(!areteDejaExistante(source, destination))
            {
                    aretes.add(new Arete(taille, Color.BLUE, source, destination));
                    System.out.println("Arete n°" + aretes.getLast());
            }
        }
        setChanged();
        notifyObservers();
    }

    public void ajouterSelection(Noeud tmp) {
        selectionNoeuds.add(tmp);
        tmp.setSelect(true);
        setChanged();
        notifyObservers();
    }
    
    public void ajouterSelectionArete(Arete tmp)
    {
        selectionAretes.add(tmp);
        tmp.setSelect(true);
        setChanged();
        notifyObservers();
        
    }
//</editor-fold>

    public void selectNoeudsMultiples(Shape rect) {
        for(int i=0; i<noeuds.size(); i++)
        {
            if(rect.contains(noeuds.get(i).getPosition()) && !noeuds.get(i).isSelect())
            {
                ajouterSelection(noeuds.get(i));
            }
            else if(!rect.contains(noeuds.get(i).getPosition()))
                retirerNoeudSelection(noeuds.get(i));
        }
        setChanged();
        notifyObservers();
    }

    public void deplacerNoeudsSelectionne(Point souris, Noeud ndT) {
        Point tmp = new Point(ndT.getPosX(),ndT.getPosY());
        int deplacementX;
        int deplacementY;
        for (Noeud noeudN : selectionNoeuds) {
            deplacementX = noeudN.getPosition().x - tmp.x;
            deplacementY = noeudN.getPosition().y - tmp.y;
            noeudN.setPosX((int)(souris.x+deplacementX));
            noeudN.setPosY((int)(souris.y+deplacementY));
        }
        setChanged();
        notifyObservers();
    }

    public void retirerNoeudSelection(Noeud tmp) {
        selectionNoeuds.remove(tmp);
        tmp.setSelect(false);
        
        setChanged();
        notifyObservers();
    }
    
    public void retirerAreteSelection(Arete tmp) {
        selectionAretes.remove(tmp);
        tmp.setSelect(false);
        setChanged();
        notifyObservers();
    }

    public String getEtiquetteSelection() {
        String noeuds = "(";
        for(int i=0; i<getSelectionNoeuds().size();i++)
        {
            if(getSelectionNoeuds().get(i).isSelect())
            {
                noeuds+=getSelectionNoeuds().get(i).getEtiquette();
            }
            if(getSelectionNoeuds().get(i) != getSelectionNoeuds().getLast())
                noeuds+= ", ";
        }
        noeuds+=")";
        return noeuds;
    }

    public void selectAretes(Shape rect) {
        for (Arete arete : aretes) {
            if (rect.contains(arete.getPosition()) && !arete.isSelect()) {
                ajouterSelectionArete(arete);
            } else if (!rect.contains(arete.getPosition())) {
                retirerAreteSelection(arete);
            }
        }
        setChanged();
        notifyObservers();
    }

    public void supNoeudSimple(Point pointPresse) {
        Iterator <Noeud> it = noeuds.iterator();
        while (it.hasNext()){
            Noeud tmp = it.next();
            if (tmp.getForme().contains(pointPresse))
            {
                retirerNoeudSelection(tmp);
                noeuds.remove(tmp);
            }
        }
        setChanged();
        notifyObservers();
    }

    public void selectionnerTout() {
        for (Arete arete : aretes) {
            if (!arete.isSelect()) {
                ajouterSelectionArete(arete);
            }
        }
        for (Noeud noeud : noeuds) {
            if (!noeud.isSelect()) {
                ajouterSelection(noeud);
            }
        }
        setChanged();
        notifyObservers();
    }

    public void selectionCompleteNoeuds() {
        for (Noeud noeud : noeuds) {
            ajouterSelection(noeud);
        }
        setChanged();
        notifyObservers(); 
    }
    
    public void selectionCompleteAretes() {
        for (Arete arete : aretes) {
            ajouterSelectionArete(arete);
        }
        setChanged();
        notifyObservers();
    }

    public void copierSelection() {
        copieNoeuds.clear();
        for (int i = 0; i < noeuds.size(); i++) {
            if(noeuds.get(i).isSelect())
            {   
                copieNoeuds.add(noeuds.get(i));
            }
        }
        setChanged();
        notifyObservers();    
    }

    public void collerSelection(Point p, Noeud ndTmp) {
        Point tmp = copieNoeuds.getFirst().getPosition();
        int deplacementX;
        int deplacementY;
        for(int i=0; i<copieNoeuds.size(); i++)
        {
            Noeud n = new Noeud(copieNoeuds.get(i));
            deplacementX = n.getPosX() - tmp.x;
            deplacementY = n.getPosY() - tmp.y;
            n.setPosX((int)(p.x+deplacementX));
            n.setPosY((int)(p.y+deplacementY));
            noeuds.add(n);
        }
        setChanged();
        notifyObservers();
    }
}