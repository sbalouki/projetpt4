///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package projetbaloumagou;
//
//import Model.Arete;
//import Model.Noeud;
//import java.util.*;
//import java.awt.*;
//import javax.swing.*;
//import java.awt.event.*;
///**
// *
// * @author sbalouki
// */
//public class Graphe extends javax.swing.JPanel implements MouseListener, MouseMotionListener, KeyListener {
//
//// <editor-fold defaultstate="collapsed" desc="Attributs de la classe Graphe">
//    private LinkedList<Noeud> listNoeud = new LinkedList<Noeud>();
//    private LinkedList<Noeud> listSelect = new LinkedList<Noeud>();
//    private LinkedList<Noeud> listNoeudsCopies = new LinkedList<Noeud>();
//    private LinkedList<Arete> listArete = new LinkedList<Arete>();
//    
//    private JPopupMenu menuDeroulant = new JPopupMenu();
//    private JMenuItem supprimer = new JMenuItem("Supprimer");
//    private JMenuItem copier = new JMenuItem("Copier");
//    private JMenuItem coller = new JMenuItem("Coller");
//    private JMenuItem tracerArete = new JMenuItem("Tracer une arete");
//    
//    private Selection rectangleDeSelection;
//    
//    private int noeudClique = 0;
//    Point clicDroit;
//    private Point positionSourisInitiale;
//    private int premierPosX = 0;
//    private int premierPosY = 0;
//    private int posXCopie = 0;
//    private int posYCopie = 0;
//    
//    //</editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="Constructeur avec Listeners">
//    public Graphe() {
//        
//        tracerArete.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent ae) {
//                dessinerArete();
//            }
//        });
//        supprimer.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent ae) {
//                supprimerNoeud();
//            }
//        });
//        copier.addActionListener(new ActionListener(){
//        @Override
//            public void actionPerformed(ActionEvent ae) {
//                copierNoeud();
//            }
//    });
//        coller.addActionListener(new ActionListener(){
//        @Override
//            public void actionPerformed(ActionEvent ae) {
//                collerNoeud();
//            }
//    });
//        this.addMouseListener(this);
//        this.addMouseMotionListener(this);
//        this.addKeyListener(this);
//    }
//    //</editor-fold>
//    
//    public boolean cliqueDansNoeud(MouseEvent me)
//    {
//        boolean dedans = false;
//        for (int i = 0; i < listNoeud.size(); i++)
//        {
//            if (me.getX() > listNoeud.get(i).getPosX() && me.getX() < listNoeud.get(i).getPosX()+20 && me.getY() > listNoeud.get(i).getPosY() && me.getY() < listNoeud.get(i).getPosY()+20) 
//            {
//                dedans = true;
//                noeudClique = i;
//            }
//        }
//        return dedans;
//    }
//
//    public boolean dansListe(LinkedList li, Object objRecherche)
//    {
//        int i = 0;
//            while(i < li.size()) {
//                if(li.get(i) == objRecherche)
//                {
//                    return true;
//                }
//                i++;                    
//            }
//        return false;
//    }
//
//// <editor-fold defaultstate="collapsed" desc="Copier Coller">
//    public void collerNoeud()
//    {
//        for (int i = 0; i < listNoeudsCopies.size(); i++) {
//            posXCopie = listNoeud.get(i).getPosX() - listNoeud.get(noeudClique).getPosX();
//            posYCopie = listNoeud.get(i).getPosY() - listNoeud.get(noeudClique).getPosY();
//            addNoeud(20, clicDroit.x+posXCopie,clicDroit.y+posYCopie, Color.BLACK);
//        }
//        repaint();
//    }
//
//    public void copierNoeud()
//    {
//        if(listNoeud.size()!=0)
//        {
//            effacerCopie();
//        }
//        for (int i = 0; i < listNoeud.size(); i++) {
//            if(listNoeud.get(i).isSelect())
//            {
//                listNoeudsCopies.add(listNoeud.get(i));
//            }
//        }
//    }//</editor-fold>
//    
//// <editor-fold defaultstate="collapsed" desc="Ajout de noeuds, d'arêtes et création de la sélection.">
//    public void addNoeud(int taille, int posX, int posY, Color col)
//    {
//        listNoeud.add(new Noeud(taille,posX-taille/2,posY-taille/2,col));
//        repaint();
//    }
//
//    public void addArete(int taille, Color col, Noeud sour, Noeud dest)
//    {
//        listArete.add(new Arete(taille, Color.BLUE, sour, dest));
//        repaint();
//    }
//
//    public void selection()
//    {
//        if(!dansListe(listSelect, listNoeud.get(noeudClique)))
//        {
//            listSelect.add(listNoeud.get(noeudClique));
//            listNoeud.get(noeudClique).setSelect(true);
//            System.out.println("Ajouté! N° : " + noeudClique + ". ");
//        }
//        else 
//        {
//            listSelect.remove(listNoeud.get(noeudClique));
//            listNoeud.get(noeudClique).setSelect(false);
//            System.out.println("Effacé! N° : " + noeudClique + ". ");
//        }
//    }
//    
//    public void deselection()
//    {
//        initSelection();
////        for (int i = 0; i < listNoeud.size(); i++) {
////            listNoeud.get(i).setSelect(false);
//            effacerSelection();
////            listSelect.remove();
////        }
//        repaint();
//    }
//    
//    
////  </editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="Initialiser et compter le nombre de noeuds sélectionnés.">
//    public void initSelection()
//    {
//        for (int i = 0; i < listNoeud.size(); i++) {
//            listNoeud.get(i).setCol(Color.BLACK);
//            listNoeud.get(i).setSelect(false);
//        }
//    }
//
//    public int getNbSelect()
//    {
//        int cpt = 0;
//        for (int i = 0; i < listNoeud.size(); i++) {
//            if(listNoeud.get(i).isSelect())
//                cpt++;
//        }
//        return cpt;
//    }//</editor-fold>
//
//    @Override
//    public void paint(Graphics g)
//    {
//        super.paint(g);
//        for (int i=0; i < listNoeud.size(); i++) {
////            listNoeud.get(i).dessine(g);
//            System.out.println("Liste Noeud : " + listNoeud.size() + " ...\n");
//            
//        }
//        
//        for (int i=0; i < listArete.size(); i++) {
//            listArete.get(i).dessine(g);
//            System.out.println("Liste Aretes : " + listArete.size() + " ...\n");
//        }
//    }
//
//    public void dessinerArete()
//    {
//        if(listSelect.size()==2)
//        {
//            addArete(100, new Color(0,0,0), listSelect.get(0), listSelect.get(1));
//            effacerSelection();
//        }
//    }
//
//    public void selectionnerTout()
//    {
//        for(int i=0; i<listNoeud.size(); i++)
//        {
//            if(!listNoeud.get(i).isSelect())
//            {
//                noeudClique=i;
//                selection();
//            }
//        }
//        repaint();
//    }
//    
//// <editor-fold defaultstate="collapsed" desc="Vider les listes">
//    
//    
//    public void effacerTout()
//    {
//        effacerCopie();
//        effacerNoeuds();
//        effacerSelection();
//        effacerArete();
//        this.repaint();
//    }
//    
//    public void effacerCopie()
//    {
//        listNoeudsCopies.clear();
//        repaint();
//    }
//    
//    public void effacerNoeuds()
//    {
//        listNoeud.clear();
//        repaint();
//    }
//    
//    public void effacerSelection()
//    {
//        listSelect.clear();
//        initSelection();
//        repaint();
//    }
//
//    public void effacerArete()
//    {
//        listArete.clear();
//        repaint();
//    }// </editor-fold>  
//
//// <editor-fold defaultstate="collapsed" desc="Supprimer des noeuds">
//    public void supprimerNoeud()
//    {
//        int limite = listNoeud.size()-1;
//        if(listNoeud.get(noeudClique).isSelect() && getNbSelect()!=0)
//        {
//            supprimerAretesApresSelection();
//            for (int i = limite; i >= 0; i--) {
//                
//                if (listNoeud.get(i).isSelect()) {
//                    listNoeud.remove(i);
//                }
//            }
//            effacerSelection();
//        }
//        else
//        {
//            supprimerAretes();
//            listNoeud.remove(noeudClique);
//        }
//        repaint();
//    }// </editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="Supprimer les arêtes">
//    public void supprimerAretes()
//    {
//        for (int i = 0; i < listArete.size(); i++) {
//                if(listNoeud.get(noeudClique) == listArete.get(i).getSource() || listNoeud.get(noeudClique) == listArete.get(i).getDestination())
//                {
//                    listArete.remove(i);
//                    i--;
//                }
//        }
//            repaint();
//    }
//    public void supprimerAretesApresSelection()
//    {
//        for (int i = 0; i < listArete.size(); i++) {
//            for (int noeudSelect = 0; noeudSelect < listNoeud.size(); noeudSelect++) {
//                if(listNoeud.get(noeudSelect).isSelect())
//                {
//                    if(i>=0)
//                    if(listNoeud.get(noeudSelect) == listArete.get(i).getSource() || listNoeud.get(noeudSelect) == listArete.get(i).getDestination())
//                    {
//                        listArete.remove(i);
//                        i--;
//                    }
//                }
//            }
//        }
//            repaint();
//    }// </editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="Evènements de la souris">
//// <editor-fold defaultstate="collapsed" desc="Clic simple">
//    @Override
//    public void mouseClicked(MouseEvent me)
//    {
//        if (SwingUtilities.isLeftMouseButton(me)) {
//            if(!cliqueDansNoeud(me))
//            {
//                addNoeud(20,me.getX(),me.getY(),Color.BLACK);
//            }
//            else if (cliqueDansNoeud(me))
//            {
//                selection();
//            }
//        }
//        if (SwingUtilities.isRightMouseButton(me)) {
//                
//            clicDroit = me.getPoint();
//            menuDeroulant.add(supprimer);
//            menuDeroulant.add(tracerArete);
//            menuDeroulant.add(copier);
//            menuDeroulant.add(coller);
//            if (cliqueDansNoeud(me))
//            {
//                listNoeud.get(noeudClique).setSelect(true); // A VOIR A VOIR A VOIR A VOIR
//                supprimer.setEnabled(true);
//                tracerArete.setEnabled(true);
//                copier.setEnabled(true);
//                coller.setEnabled(false);
//            }
//            else
//            {
//                supprimer.setEnabled(false);
//                tracerArete.setEnabled(false);
//                copier.setEnabled(false);
//                if(listNoeudsCopies.size()>0)
//                    coller.setEnabled(true);
//                else
//                    coller.setEnabled(false);
//            }
//            menuDeroulant.show(this, me.getX(), me.getY());
//            this.setVisible(true);
//        }
//        repaint();
//    }
////</editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="MousePressed">
//    @Override
//    public void mousePressed(MouseEvent me) {
//       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
////        if(cliqueDansNoeud(me) && listNoeud.get(noeudClique).isSelect()){
//            positionSourisInitiale = me.getPoint();
////        }
//    }//</editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="Bouton de la souris relaché">
//    @Override
//    public void mouseReleased(MouseEvent me) {
//       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }//</editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="Entrée du curseur dans la fenetre">
//    @Override
//    public void mouseEntered(MouseEvent me) {
//       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }//</editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="Sortie du curseur dans la fenetre">
//    @Override
//    public void mouseExited(MouseEvent me) {
//       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }//</editor-fold>
//
//// <editor-fold defaultstate="collapsed" desc="Drag and drop">
//    @Override
//    public void mouseDragged(MouseEvent me) {
////        System.out.println("Déplacement du noeud enclenché...");
//        Point positionSourisFinale = me.getPoint();
//        if(cliqueDansNoeud(me))
//        {
//            if(getNbSelect()<=1 || !listNoeud.get(noeudClique).isSelect())
//            {
//                listNoeud.get(noeudClique).setPosX(me.getX()-10);
//                listNoeud.get(noeudClique).setPosY(me.getY()-10);
//                repaint();
//            }
//            else
//            {
//                int deplacementX = positionSourisFinale.x-positionSourisInitiale.x;
//                int deplacementY = positionSourisFinale.y-positionSourisInitiale.y;
//                for (Noeud listNoeud1 : listNoeud) {
//                    if (listNoeud1.isSelect()) {
//                        positionSourisInitiale.x = listNoeud1.getPosX();
//                        positionSourisInitiale.y = listNoeud1.getPosY();
//                        listNoeud1.setPosX(positionSourisInitiale.x+deplacementX);
//                        listNoeud1.setPosY(positionSourisInitiale.y+deplacementY);
//                        positionSourisInitiale = me.getPoint();
//                    }
//                    repaint();
//                }
//            }
//        }
//        else
//        {
////            drawRect(positionSourisInitiale.x, positionSourisInitiale.y, positionSourisFinale.x, positionSourisFinale.y);
//        }
//    }
////</editor-fold>
//    
////<editor-fold defaultstate="collapsed" desc="Souris qui bouge">
//    @Override
//    public void mouseMoved(MouseEvent me) {
////            System.out.println("Déplacement du noeud terminé...");
//    }//</editor-fold>
////</editor-fold>
//    
//// <editor-fold defaultstate="collapsed" desc="Evenements de clavier">
//    @Override
//    public void keyTyped(KeyEvent ke) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    
//    }
//
//    @Override
//    public void keyPressed(KeyEvent ke) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//  //  System.out.print("ta mere");
//    }
//
//    @Override
//    public void keyReleased(KeyEvent ke) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//
//}//</editor-fold>
