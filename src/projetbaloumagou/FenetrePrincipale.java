/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package projetbaloumagou;

import Controleur.Controleur;
import Model.Model;
import View.View;
import View.View;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author sbalouki
 */

public class FenetrePrincipale extends javax.swing.JFrame implements Observer{ 
    // <editor-fold defaultstate="collapsed" desc="Déclaration des variables">
//    private View vue ;
    private Model _model;
    private boolean modeDeplacement = false;
    private Controleur control;
    private String messageSauvegarder = "Voulez-vous sauvegarder avant de créer un nouveau graphe?";
    private String messageQuitter = "Voulez-vous sauvegarder avant de quitter?";
    private String messageCharger = "Voulez-vous sauvegarder avant de charger?";
    private String messageEffacerGraphe = "Êtes-vous certain-e de vouloir effacer le graphe?";
    private boolean chargeAnnul = false;
    //</editor-fold>
    /**
     * Creates new form Menu
     */
    // <editor-fold defaultstate="collapsed" desc="Constructeur">
    public FenetrePrincipale(Model mod, View _vue, Controleur cont) {
        setVisible(true);
        control =  cont;
        panelGraphe = _vue;
        _model=mod;
        _model.addObserver(this);
        initRealComponents();
        
    }//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Fonctions de sauvegarde et de chargement">    
    public void sauvegarder()
    {
            JFileChooser menuEnreg=new JFileChooser();
            menuEnreg.showDialog(this, "Enregistrer");
            menuEnreg.setVisible(true);
            if(menuEnreg.isEnabled())
            {
                chargeAnnul=true;
            }
    }

    private void charger() {
        JFileChooser menuCharger=new JFileChooser();
        menuCharger.showDialog(this, "Charger");
        menuCharger.setVisible(true);
    }
    // </editor-fold>
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="InitComponents utilisable">                          
    private void initRealComponents() {
        
        formeNoeud = new javax.swing.JComboBox();
        formeLabel = new javax.swing.JLabel();
        tailleNoeud = new javax.swing.JSpinner();
        tailleLabel = new javax.swing.JLabel();
        modeLabel = new javax.swing.JLabel();
        modeNoeud = new javax.swing.JToggleButton();
        annulerSelection = new javax.swing.JButton();
        etiquetteNoeud = new javax.swing.JTextField();
        etiquetteLabel = new javax.swing.JLabel();
        supprimerSelection = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        menuNouveau = new javax.swing.JMenuItem();
        menuQuitter = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        EditionSelectionnerTout = new javax.swing.JMenuItem();
        EditionSelectAretes = new javax.swing.JMenuItem();
        EditionSelectNoeuds = new javax.swing.JMenuItem();
        EditionSupprimer = new javax.swing.JMenuItem();
        EditionSupprimerGraphe = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Graphes");
        setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout panelGrapheLayout = new javax.swing.GroupLayout(panelGraphe);
        panelGraphe.setLayout(panelGrapheLayout);
        panelGrapheLayout.setHorizontalGroup(
            panelGrapheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 292, Short.MAX_VALUE)
        );
        panelGrapheLayout.setVerticalGroup(
            panelGrapheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        formeNoeud.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Rectangle", "Triangle", "Rond" }));
        formeNoeud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                formeNoeudActionPerformed(evt);
            }
        });

        formeLabel.setText("Forme");

        tailleNoeud.setModel(new javax.swing.SpinnerNumberModel(20, 20, 50, 1));
        tailleNoeud.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tailleNoeudStateChanged(evt);
            }
        });

        tailleLabel.setText("Taille");

        modeLabel.setText("Mode");

        modeNoeud.setText("Deplacement");
        modeNoeud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modeNoeudActionPerformed(evt);
            }
        });

        annulerSelection.setText("Annuler la sélection");
        annulerSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerSelectionActionPerformed(evt);
            }
        });

        etiquetteNoeud.setText(control.getEtiquetteSelectionNoeud());
        etiquetteNoeud.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                etiquetteNoeudKeyTyped(evt);
            }
        });

        etiquetteLabel.setText("Selection");

        supprimerSelection.setText("Supprimer Selection");
        supprimerSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supprimerSelectionActionPerformed(evt);
            }
        });

        fileMenu.setText("File");

        menuNouveau.setText("Nouveau");
        menuNouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuNouveauActionPerformed(evt);
            }
        });
        fileMenu.add(menuNouveau);

        menuQuitter.setText("Quitter");
        menuQuitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuQuitterActionPerformed(evt);
            }
        });
        fileMenu.add(menuQuitter);

        jMenuBar1.add(fileMenu);

        editMenu.setText("Edit");

        EditionSelectionnerTout.setText("Sélectionner tout");
        EditionSelectionnerTout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditionSelectionnerToutActionPerformed(evt);
            }
        });
        editMenu.add(EditionSelectionnerTout);

        EditionSelectAretes.setText("Sélectionner Aretes");
        EditionSelectAretes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditionSelectAretesActionPerformed(evt);
            }
        });
        editMenu.add(EditionSelectAretes);

        EditionSelectNoeuds.setText("Selectionner Noeuds");
        EditionSelectNoeuds.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditionSelectNoeudsActionPerformed(evt);
            }
        });
        editMenu.add(EditionSelectNoeuds);

        EditionSupprimer.setText("Supprimer Sélection");
        EditionSupprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditionSupprimerActionPerformed(evt);
            }
        });
        editMenu.add(EditionSupprimer);

        EditionSupprimerGraphe.setText("Supprimer Graphe");
        EditionSupprimerGraphe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditionSupprimerGrapheActionPerformed(evt);
            }
        });
        editMenu.add(EditionSupprimerGraphe);

        jMenuBar1.add(editMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelGraphe, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(42, 42, 42)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(formeLabel)
                                    .addComponent(tailleLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(modeLabel))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(etiquetteLabel)
                                    .addGap(4, 4, 4)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(19, 19, 19)
                                    .addComponent(modeNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(tailleNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(formeNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(etiquetteNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(annulerSelection, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(supprimerSelection, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGraphe, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(formeNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(formeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tailleNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tailleLabel))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(modeNoeud)
                    .addComponent(modeLabel))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(etiquetteNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetteLabel))
                .addGap(18, 18, 18)
                .addComponent(annulerSelection)
                .addGap(18, 18, 18)
                .addComponent(supprimerSelection)
                .addGap(0, 158, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelGraphe = new javax.swing.JPanel();
        formeNoeud = new javax.swing.JComboBox();
        formeLabel = new javax.swing.JLabel();
        tailleNoeud = new javax.swing.JSpinner();
        tailleLabel = new javax.swing.JLabel();
        modeLabel = new javax.swing.JLabel();
        modeNoeud = new javax.swing.JToggleButton();
        annulerSelection = new javax.swing.JButton();
        etiquetteNoeud = new javax.swing.JTextField();
        etiquetteLabel = new javax.swing.JLabel();
        supprimerSelection = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        menuNouveau = new javax.swing.JMenuItem();
        menuQuitter = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        EditionSelectionnerTout = new javax.swing.JMenuItem();
        EditionSelectAretes = new javax.swing.JMenuItem();
        EditionSelectNoeuds = new javax.swing.JMenuItem();
        EditionSupprimer = new javax.swing.JMenuItem();
        EditionSupprimerGraphe = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Graphes");
        setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout panelGrapheLayout = new javax.swing.GroupLayout(panelGraphe);
        panelGraphe.setLayout(panelGrapheLayout);
        panelGrapheLayout.setHorizontalGroup(
            panelGrapheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 292, Short.MAX_VALUE)
        );
        panelGrapheLayout.setVerticalGroup(
            panelGrapheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        formeNoeud.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Rectangle", "Triangle", "Rond" }));
        formeNoeud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                formeNoeudActionPerformed(evt);
            }
        });

        formeLabel.setText("Forme");

        tailleNoeud.setModel(new javax.swing.SpinnerNumberModel(20, 20, 50, 1));
        tailleNoeud.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tailleNoeudStateChanged(evt);
            }
        });

        tailleLabel.setText("Taille");

        modeLabel.setText("Mode");

        modeNoeud.setText("Deplacement");
        modeNoeud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modeNoeudActionPerformed(evt);
            }
        });

        annulerSelection.setText("Annuler la sélection");
        annulerSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerSelectionActionPerformed(evt);
            }
        });

        etiquetteNoeud.setText(control.getEtiquetteSelectionNoeud());
        etiquetteNoeud.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                etiquetteNoeudKeyTyped(evt);
            }
        });

        etiquetteLabel.setText("Selection");

        supprimerSelection.setText("Supprimer Selection");
        supprimerSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supprimerSelectionActionPerformed(evt);
            }
        });

        fileMenu.setText("File");

        menuNouveau.setText("Nouveau");
        menuNouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuNouveauActionPerformed(evt);
            }
        });
        fileMenu.add(menuNouveau);

        menuQuitter.setText("Quitter");
        menuQuitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuQuitterActionPerformed(evt);
            }
        });
        fileMenu.add(menuQuitter);

        jMenuBar1.add(fileMenu);

        editMenu.setText("Edit");

        EditionSelectionnerTout.setText("Sélectionner tout");
        EditionSelectionnerTout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditionSelectionnerToutActionPerformed(evt);
            }
        });
        editMenu.add(EditionSelectionnerTout);

        EditionSelectAretes.setText("Sélectionner Aretes");
        EditionSelectAretes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditionSelectAretesActionPerformed(evt);
            }
        });
        editMenu.add(EditionSelectAretes);

        EditionSelectNoeuds.setText("Selectionner Noeuds");
        EditionSelectNoeuds.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditionSelectNoeudsActionPerformed(evt);
            }
        });
        editMenu.add(EditionSelectNoeuds);

        EditionSupprimer.setText("Supprimer Sélection");
        EditionSupprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditionSupprimerActionPerformed(evt);
            }
        });
        editMenu.add(EditionSupprimer);

        EditionSupprimerGraphe.setText("Supprimer Graphe");
        EditionSupprimerGraphe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditionSupprimerGrapheActionPerformed(evt);
            }
        });
        editMenu.add(EditionSupprimerGraphe);

        jMenuBar1.add(editMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelGraphe, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(42, 42, 42)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(formeLabel)
                                    .addComponent(tailleLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(modeLabel))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(etiquetteLabel)
                                    .addGap(4, 4, 4)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(19, 19, 19)
                                    .addComponent(modeNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(tailleNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(formeNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(etiquetteNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(annulerSelection, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(supprimerSelection, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGraphe, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(formeNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(formeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tailleNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tailleLabel))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(modeNoeud)
                    .addComponent(modeLabel))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(etiquetteNoeud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetteLabel))
                .addGap(18, 18, 18)
                .addComponent(annulerSelection)
                .addGap(18, 18, 18)
                .addComponent(supprimerSelection)
                .addGap(0, 158, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    
    private void formeNoeudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_formeNoeudActionPerformed
        control.setFormeParDefaut(formeNoeud.getSelectedItem().toString());
    }//GEN-LAST:event_formeNoeudActionPerformed

    private void tailleNoeudStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tailleNoeudStateChanged
        control.setTailleParDefaut((int)tailleNoeud.getValue());
    }//GEN-LAST:event_tailleNoeudStateChanged

    private void modeNoeudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modeNoeudActionPerformed
        // TODO add your handling code here:
        control.setMode(!control.getMode());
    }//GEN-LAST:event_modeNoeudActionPerformed

    private void menuNouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuNouveauActionPerformed
        // TODO add your handling code here:
        int enreg = JOptionPane.showConfirmDialog(this, messageSauvegarder, "Enregistrer",JOptionPane.YES_NO_CANCEL_OPTION);
        if(enreg == JOptionPane.OK_OPTION)
        {
           this.sauvegarder();
        }
        else if(enreg == JOptionPane.NO_OPTION)
        {
            control.reinitialiser();
            formeNoeud.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Rectangle", "Triangle", "Rond" }));
            tailleNoeud.setModel(new javax.swing.SpinnerNumberModel(20, 20, 50, 1));
        }
//        tailleNoeud.setValue(20);
//        formeNoeud.setSelectedItem("Rectangle");
    }//GEN-LAST:event_menuNouveauActionPerformed

    private void menuQuitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuQuitterActionPerformed
        // TODO add your handling code here:
        int quitte = JOptionPane.showConfirmDialog(this, messageQuitter, "Quitter",JOptionPane.YES_NO_CANCEL_OPTION);
        if(quitte == JOptionPane.OK_OPTION)
        {
            sauvegarder();
        }
        else if(quitte == JOptionPane.NO_OPTION)
        {
            this.dispose();
        }
    }//GEN-LAST:event_menuQuitterActionPerformed

    private void EditionSupprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditionSupprimerActionPerformed
        // TODO add your handling code here:
        control.supprimerObjSelect();
    }//GEN-LAST:event_EditionSupprimerActionPerformed

    private void annulerSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_annulerSelectionActionPerformed
        // TODO add your handling code here:
        control.initSelection();
    }//GEN-LAST:event_annulerSelectionActionPerformed

    private void etiquetteNoeudKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_etiquetteNoeudKeyTyped
        // TODO add your handling code here:
        etiquetteNoeud.getText();
    }//GEN-LAST:event_etiquetteNoeudKeyTyped

    private void EditionSelectAretesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditionSelectAretesActionPerformed
        // TODO add your handling code here:
        control.selecionCompleteAretes();
    }//GEN-LAST:event_EditionSelectAretesActionPerformed

    private void EditionSelectNoeudsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditionSelectNoeudsActionPerformed
        // TODO add your handling code here:
        control.selectionCompleteNoeuds();
    }//GEN-LAST:event_EditionSelectNoeudsActionPerformed

    private void EditionSupprimerGrapheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditionSupprimerGrapheActionPerformed
        // TODO add your handling code here:
        int effacerGraphe = JOptionPane.showConfirmDialog(this, messageEffacerGraphe, "Effacer Graphe",JOptionPane.YES_NO_CANCEL_OPTION);
        if(effacerGraphe == JOptionPane.OK_OPTION)
        {
           control.reinitialiser();
           formeNoeud.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Rectangle", "Triangle", "Rond" }));
           tailleNoeud.setModel(new javax.swing.SpinnerNumberModel(20, 20, 50, 1));
           control.setMode(false);
           modeNoeud.setSelected(false);
        }
    }//GEN-LAST:event_EditionSupprimerGrapheActionPerformed

    private void supprimerSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supprimerSelectionActionPerformed
        // TODO add your handling code here:
        control.supprimerObjSelect();
    }//GEN-LAST:event_supprimerSelectionActionPerformed

    private void EditionSelectionnerToutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditionSelectionnerToutActionPerformed
        // TODO add your handling code here:
        control.selectionComplete();
    }//GEN-LAST:event_EditionSelectionnerToutActionPerformed
    /**
     * @param args the command line arguments
     */
    // <editor-fold defaultstate="collapsed" desc="Main">
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FenetrePrincipale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FenetrePrincipale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FenetrePrincipale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FenetrePrincipale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
                Model model = new Model();
                Controleur control = new Controleur(model);
                View vue = new View(model, control);
                new FenetrePrincipale(model, vue, control);
            }
        });
    }//</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Déclaration des variables javax">    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem EditionSelectAretes;
    private javax.swing.JMenuItem EditionSelectNoeuds;
    private javax.swing.JMenuItem EditionSelectionnerTout;
    private javax.swing.JMenuItem EditionSupprimer;
    private javax.swing.JMenuItem EditionSupprimerGraphe;
    private javax.swing.JButton annulerSelection;
    private javax.swing.JMenu editMenu;
    private javax.swing.JLabel etiquetteLabel;
    private javax.swing.JTextField etiquetteNoeud;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JLabel formeLabel;
    private javax.swing.JComboBox formeNoeud;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem menuNouveau;
    private javax.swing.JMenuItem menuQuitter;
    private javax.swing.JLabel modeLabel;
    private javax.swing.JToggleButton modeNoeud;
    private javax.swing.JPanel panelGraphe;
    private javax.swing.JButton supprimerSelection;
    private javax.swing.JLabel tailleLabel;
    private javax.swing.JSpinner tailleNoeud;
    // End of variables declaration//GEN-END:variables
    //</editor-fold>

    @Override
    public void update(Observable o, Object o1) {
        etiquetteNoeud.setText(control.getEtiquetteSelectionNoeud());
    }
}
