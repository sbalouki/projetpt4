/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controleur;

import Model.Arete;
import Model.Model;
import Model.Noeud;
import java.awt.Point;
import java.awt.Shape;
import java.util.LinkedList;

/**
 *
 * @author sbalouki
 */

public class Controleur {
    // <editor-fold defaultstate="collapsed" desc="Déclaration de variables">
    Model _model;
   public Noeud tmp;
   public Arete artTmp;


    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Constructeur">
    public Controleur(Model mod)
    {
        _model = mod;
    }
//</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters">
    public LinkedList<Noeud> getNoeuds()
    {
        return _model.getNoeuds();
    }
    
    public LinkedList<Noeud> getNoeudsCopie()
    {
        return _model.getCopieNoeuds();
    }
    
    public boolean getMode() {
        return _model.isModeDeplacement();
    }
    
    public String getEtiquetteSelectionNoeud()
    {
        return _model.getEtiquetteSelection();
    }

    public boolean isMovable() {
        return tmp != null;
    }
    
    public boolean dansNoeud(Point pointClique)
    {
        return _model.cliqueDansNoeud(pointClique);
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Setters">
    public void setFormeParDefaut(String forme)
    {
        _model.setForme(forme);
    }
    
    public void setArtTmp(Arete _artTmp)
    {
        this.artTmp = _artTmp;
    }
    
    public void setTmp(Noeud _tmp) {
        this.tmp = _tmp;
    }
    
    public void setTailleParDefaut(int taille)
    {
        if(_model.getSelectionNoeuds().isEmpty())
        {
            System.out.println("Selection de " + _model.getSelectionNoeuds().size());
            _model.setTaille(taille);
        }
        else
            _model.setTailleSelection(taille);
    }
    
    public void setMode(boolean mode) {
        _model.setModeDeplacement(mode);
    }
//</editor-fold>

    public void reinitialiser() {
        _model.initialiser();
    }

    public void supprimerObjSelect() {
        _model.supNoeud();
        _model.supArete();
    }

    public void selectionMultiple(Shape rect) {
        _model.selectNoeudsMultiples(rect);
        _model.selectAretes(rect);
    }

    public void initSelection() {
        _model.initSelection();
    }

    public void selectionNoeudSimple(){
        if(!tmp.isSelect())
        {
            _model.ajouterSelection(tmp);
        }
        else
            _model.retirerNoeudSelection(tmp);
    }

    public void selectionComplete()
    {
        _model.selectionnerTout();
    }

    public void clique(Point point) {
        _model.ajouterNoeud(point);
    }

    public void presse(Point point) {
            tmp = _model.noeudChoisi(point);
    }

    public void tracerArete(Point destination) {
        Noeud dest = _model.noeudChoisi(destination);
        if(tmp != null && destination != null)
            _model.ajouterArete(tmp, dest);
    }

    public void deplacement(Point souris) {
        if(!_model.getSelectionNoeuds().isEmpty() && tmp!=null && tmp.isSelect())
            _model.deplacerNoeudsSelectionne(souris,tmp);
    }

    public boolean arete() {
        if(_model.getSelectionNoeuds().size()==2)
            return true;
        else
            return false;
    }

    public boolean surArete(Point pointClique) {
        artTmp = _model.areteChoisie(pointClique);
        return _model.cliqueSurArete(pointClique);
    }

    public void selectionAreteSimple() {
        if(!artTmp.isSelect())
        {
            _model.ajouterSelectionArete(artTmp);
        }
        else
            _model.retirerAreteSelection(artTmp);
    }

    public void supprimerObjClique(Point pointPresse) {
        _model.supNoeudSimple(pointPresse);
        
    }

    public void selecionCompleteAretes() {
        _model.selectionCompleteAretes();
    }

    public void selectionCompleteNoeuds() {
        _model.selectionCompleteNoeuds();
    }

    public void copierSelection() {
        _model.copierSelection();
    }

    public void collerSelection(Point p) {
        if(!_model.getSelectionNoeuds().isEmpty() && tmp!=null)
            _model.collerSelection(p, tmp);
    }
}
